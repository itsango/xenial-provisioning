#! /bin/bash

(curl -L https://install.perlbrew.pl | bash) && (
	echo 'source ~/perl5/perlbrew/etc/bashrc' >>~/.bashrc
	source ~/perl5/perlbrew/etc/bashrc
	perlbrew init
	)
