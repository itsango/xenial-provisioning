#! /bin/sh

provlibdir="$HOME/lib/prov.d/lib/"
packerplugindir="$HOME/.packer.d/plugins"

mkdir -p "$packerplugindir"
unzip "$provlibdir/packer-builder-sakuracloud_linux-amd64.zip" -d "$packerplugindir"
