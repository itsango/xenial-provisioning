#! /bin/sh

. "$HOME/lib/prov.d/prov.conf"

unzip "$provlibdir"/Aptana_Studio_*_Setup_Linux_x86_64_*.zip

rm -rf "$HOME"/lib/Aptana_Studio*
mv -i Aptana_Studio* "$HOME/lib"

ln -s "$HOME"/lib/Aptana_Studio*/AptanaStudio3 "$HOME/bin"

cat <<EOF >"$HOME/.local/share/applications/AptanaStudio3.desktop"
[Desktop Entry]
Name=AptanaStudio3
Exec=aptanaStudio3
Type=Application
Icon=$HOME/lib/Aptana_Studio_3/icon.xpm
EOF
