#! /bin/bash -u

nvm_dir="$HOME/.nvm"


install_nvm() {
	git clone 'git@github.com:MItsutoshiNAKANO/nvm.git' "$nvm_dir" &&
		cd  "$nvm_dir" &&
		git remote add upstream 'git@github.com:nvm-sh/nvm.git' &&
		git pull upstream &&
		echo 'configure ~/.bash_profile, see https://github.com/nvm-sh/nvm' >&2
}

update_nvm() {
	cd "$nvm_dir" &&
		(git checkout master ||
			true) &&
		git fetch upstream master &&
		git merge upstream/master &&
		git push origin master
}


if [ -d "$HOME/.nvm" ]
then
	update_nvm && . "$NVM_DIR/nvm.sh"
else
	install_nvm
fi
