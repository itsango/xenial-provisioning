#! /bin/sh
provtmpdir="$HOME/lib/prov.d/tmp/"


sed 's/^DEFAULT_FORWARD_POLICY=.*$/DEFAULT_FORWARD_POLICY="ACCEPT"/' /etc/default/ufw >"$provtmpdir/ufw"

sudo sh -c "cat $provtmpdir/ufw >/etc/default/ufw"
