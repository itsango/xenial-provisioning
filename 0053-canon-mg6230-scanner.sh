#! /bin/sh

provlibdir="$HOME/lib/prov.d/lib/"

tar xf "$provlibdir/scangearmp-mg6200series-1.80-1-deb.tar.gz"
cd scangearmp-mg6200series-1.80-1-deb
sudo ./install.sh
cd ..
rm -rf scangearmp-mg6200series-1.80-1-deb 

## see http://qiita.com/SUZUKI_Masaya/items/84269bfeeda538429feb
sudo gpasswd -a "$USER" scanner
