#! /bin/sh

curl --proto '=https' --tlsv1.2 -sSf 'https://sh.rustup.rs' | sh

# @see https://www.rust-lang.org/ja/learn/get-started
# @see https://qiita.com/yoshiyasu1111/items/0c3d08658560d4b91431
