#! /bin/sh

. "$HOME/lib/prov.d/prov.conf"

target='0002-Jenkins.list'

cat >"$target" <<EOF
deb https://pkg.jenkins.io/debian-stable binary/
EOF

sudo install -m 644 "$target" /etc/apt/sources.list.d/

rm "$target"
