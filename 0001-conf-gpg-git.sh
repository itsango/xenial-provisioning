#! /bin/sh

provlibpath="$HOME/lib/prov.d/"

gpg --import "$provlibpath"/sec*.asc
gpg --import "$provlibpath"/pub*.asc"

