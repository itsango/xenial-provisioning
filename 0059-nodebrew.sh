#! /bin/sh

. "$HOME/lib/prov.d/prov.conf"

perl "$provlibdir/nodebrew" setup

egrep -l '\.nodebrew' "$HOME/.bashrc" ||
	echo 'export PATH=$HOME/.nodebrew/current/bin:$PATH' >>"$HOME/.bashrc"

echo "Please reboot your shell!"
