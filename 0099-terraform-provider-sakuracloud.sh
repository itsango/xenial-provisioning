#! /bin/sh

provdir="$HOME/lib/prov.d/"
provlibdir="$provdir/lib/"
. "$provdir/prov.conf"

name='terraform-provider-sakuracloud'

mkdir "$bindir"
unzip "$provlibdir/${name}_*_linux-amd64.zip" -d "$bindir"

