#! /bin/sh

. "$HOME/lib/prov.d/prov.conf"

tar xf "$provlibdir"/spring-tool-suite-*-linux.gtk.x86_64.tar.gz

rm -rf "$libdir"/sts-*

mv sts-* "$libdir"

rm -f "$bindir/SpringToolSuite4"
ln -s "$libdir"/sts-*/SpringToolSuite4 "$bindir"

iconfile=`echo "$libdir"/sts-*/icon.xpm`

cat <<EOF >"$HOME/.local/share/applications/SpringToolSuite4.desktop"
[Desktop Entry]
Name=SpringToolSuite4
Exec=SpringToolSuite4
Type=Application
Icon=$iconfile
EOF
