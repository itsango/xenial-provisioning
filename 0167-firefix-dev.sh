#! /bin/sh

target="$HOME/lib/firefox"

rm -rf "$target"

tar xf lib/firefox-*.tar.bz2 &&
	mv -i firefox "$target" &&
	ln -s "$target/firefox" "$HOME/bin/firefox"
