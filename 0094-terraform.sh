#! /bin/sh

provdir="$HOME/lib/prov.d/"
provlibdir="$provdir/lib/"
. "$provdir/prov.conf"

name='terraform'

mkdir "$bindir"
unzip "$provlibdir/${name}_*_linux_amd64.zip" -d "$bindir"

