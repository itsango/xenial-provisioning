#! /bin/sh

vagrant plugin install sahara
#vagrant plugin install vagrant-global-status
vagrant plugin install vagrant-omnibus
vagrant plugin install vagrant-cachier
vagrant plugin install vagrant-exec
vagrant plugin install vagrant-persistent-storage
vagrant plugin install vagrant-share
vagrant plugin install vagrant-sakura
