#! /bin/sh -eu

. "$HOME/lib/prov.d/prov.conf"

rm -f "$bindir/gibo"

cd "$libdir"
git clone  'git@github.com:simonwhitaker/gibo.git'
ln -s "$libdir/gibo/gibo" "$bindir"
