#! /bin/sh

. "$HOME/lib/prov.d/prov.conf"

tar xf "$provlibdir"/eclipse-*-linux-*.tar.gz

rm -rf "$HOME/lib/eclipse"
mv eclipse "$HOME/lib"
ln -s "$HOME/lib/eclipse/eclipse" ~/bin

cat <<EOF >"$HOME/.local/share/applications/eclipse.desktop"
[Desktop Entry]
Name=Eclipse
Exec=eclipse
Type=Application
Icon=$HOME/lib/eclipse/icon.xpm
EOF
