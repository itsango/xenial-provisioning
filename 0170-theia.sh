#! /bin/sh -eu

local_repository_base="$HOME/src/repos/Mitsutoshi"
application_name='theia'

nodebrew use 10

build_theia() {
	yarn && yarn run rebuild:electron
		
}

install_theia() {
	cd "$local_repository_base" &&
		git clone git@github.com:MItsutoshiNAKANO/theia.git &&
		cd "$application_name" &&
		git remote add upstream git@github.com:nvm-sh/nvm.git &&
		git fetch upstream master &&
		git merge &&
		build_theia
}

update__theia() {
	cd "$local_repository_base/$application_name" &&
		(git checkout master ||
			true) &&
		git fetch upstream master &&
		git merge &&
		build_theia
}

if [ -d "$local_repository_base/$application_name" ]
then
	update__theia
else
	install_theia
fi
