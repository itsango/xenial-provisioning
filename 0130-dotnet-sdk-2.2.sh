#! /bin/sh
# see https://dotnet.microsoft.com/download/linux-package-manager/ubuntu18-04/sdk-2.2.401

sudo add-apt-repository universe
sudo apt install apt-transport-https
sudo apt update
sudo apt install -y dotnet-sdk-2.2
